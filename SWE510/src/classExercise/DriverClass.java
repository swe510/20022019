package classExercise;

public class DriverClass {

	public static void main(String[] args) {
		Square s = new Square("Square 1", "white",4.2);
		s.draw();
		
		Point centerofCircle = new Point("Circle Point", "black", 1, 1);
		Circle c = new Circle("Circle 1" , "red", 4,centerofCircle );
		c.draw();
		
		Rectangle r  = new Rectangle("Rectangle 1", "green", 152.2, 121.1);
		r.draw();
		
		Square s2 = new Square("Square 2", "blue", 150.4);
		s2.draw();
		
		Point startPointofSq = new Point("Square Point", "green", 10, 1);
		Square s3 = new Square("Square 3","red",10.55,startPointofSq);
		s3.draw();
		startPointofSq.draw();
		Point startPointofRec = new Point("Rec Point", "red", 15, 10);

		Rectangle r2 = new Rectangle("Rec 2","blue",100.2,25.1,startPointofRec);
		r2.draw();
	}

}
