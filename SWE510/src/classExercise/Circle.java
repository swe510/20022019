package classExercise;

public class Circle extends CircularShape{
	private double radius =0.0;
	private Point center;
	public Circle(String name, String color, double radius, Point center) {
		super(name, color);
		this.radius = radius;
		this.center = center;
	}
	
	double getArea() {
		return radius*radius;
		
	}

	@Override
	void draw() {
		// TODO Auto-generated method stub
		super.draw();
		System.out.println("I have a radius: "+radius+" and center of x: "+center.x+" center of y: "+center.y+" also, my area is:"+getArea());
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public void setCenter(Point center) {
		this.center = center;
	}
	

}
