package classExercise;

public class Shape {
	private String name="Shape";
	private String color="black";
	void draw() {
		
		System.out.println("Hi there! I'm "+name+"! I have "+color+" color");
		
	}
	public Shape(String name, String color) {
		super();
		this.name = name;
		this.color = color;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	
	
	
	
}

