package classExercise;

public class Rectangle extends Shape {
	private double width,height =0;
	private int edgeNumber =4;
	private Point startPoint;
/*TODO 
 * override draw*/
	public Rectangle(String name, String color, double width, double height) {
		super(name, color);
		this.width = width;
		this.height = height;
	}



	public Rectangle(String name, String color, double width, double height, Point startPoint) {
		super(name, color);
		this.width = width;
		this.height = height;
		this.startPoint = startPoint;
	}
	@Override
	void draw() {
		super.draw();
		System.out.println("I have  "+edgeNumber+" edges and area is "+getArea());
		System.out.println("My width and height are: "+width+"-"+height);
		if(startPoint != null)
			System.out.println(" I'm starting at: "+startPoint.x+"-"+startPoint.y);
	}
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	
	public Point getStartPoint() {
		return startPoint;
	}



	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}



	public double getArea() {
		return getHeight()*getWidth();
	}
	
	
	
	
	


}
